export SETUP_PACKAGES="rtc_cafe_signaller"
export SETUP_DESCRIPTION="rtc-cafe signaller"
export SETUP_KEYWORDS="rtc-cafe flask-socketio socketio webrtc"
export SETUP_INSTALL_REQUIRES="requirements/base.txt"

python3 -m setuppy_generator > setup.py
python3 setup.py sdist bdist_wheel